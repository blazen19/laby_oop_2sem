#include <iostream>

using namespace std;
struct Movie 
{ 
	int id;
	char* title;
	int year; 
	Movie *next; 
}; 

struct Movie* head = NULL;   

void add_Movie(int id, char *title, int year) 
{
	Movie* tmp = new Movie;
	int i=0;
	tmp->id = id;
	tmp->title = title;
	tmp->year = year;
	Movie* ptr;
	
	ptr = head;
	while (ptr != NULL) 
	{ 
		if(ptr->id==id)
		{
			cerr<<"Two movies can't have the same id\n";
			return;
		}
		ptr = ptr->next; 
	}
	
	ptr = head;
	while (ptr != NULL && year>=ptr->year) 
	{
		i++;
		ptr = ptr->next;
	} 
	
	ptr = head;
	for(int j=0; j<i-1; j++)
		ptr = ptr->next;
	
	if(i==0)
	{
		tmp->next = head; 
   		head = tmp;
		return;
	}
	if(ptr != NULL)
	{ 
		tmp->next = ptr->next; 
		ptr->next = tmp;
   		return;
	}
}

void remove_Movie(int id)
{
	Movie *ptr = head;
	if(head->id == id)
	{
		head = head->next;
		delete(ptr->title);
		delete(ptr);
		return;
	}
	
	int i=0;
	while(ptr->id != id)
	{
		ptr = ptr->next;
		i++;
		
		if(ptr == NULL)
		{
			cerr<<"There is no movie with such id"<<endl;	
			return;
		}
	}
	
	ptr = head;
	for(i; i>1; i--)
		ptr = ptr->next;
	Movie *ptr2;
	ptr2 = ptr->next;
	ptr->next = ptr2->next;
	delete(ptr2->title);
	delete (ptr2);
}

void print() 
{ 
	Movie* ptr;
	ptr = head;
	cout<<endl;	
	cout<<"The list:"<<endl;
	cout<<endl;
	while(ptr != NULL) 
	{ 
		cout<<"Id: "<< ptr->id <<endl<<"Title: "<< ptr->title<<endl<<"Year: "<<ptr->year<<endl;
		cout<<endl; 
		ptr = ptr->next; 
	}
} 

int main() 
{
	int c=1;
	
	int id, year;
	char *name;
	
	while(c)
	{
		cout<<"What would you like to do?"<<endl<<"1 - Add a movie"<<endl<<"2 - Remove a movie"<<endl<<"3 - Print the list"<<endl<<"0 - Exit"<<endl<<"Choise: ";
		cin>>c;
		
		if(c == 1)
		{
			cout<<"Id: ";
			cin>>id;
			cout<<"Title: ";
			name = new char[50];
			cin>>name;
			cout<<"Year: ";
			cin>>year;
			add_Movie(id, name, year);
		}
		
		if(c == 2)
		{
			int id;
			cout<<"Id: ";
			cin>>id;
			remove_Movie(id);
		}
		if(c == 3)
			print();
		cout<<endl;
	}
		
	return 0; 
} 