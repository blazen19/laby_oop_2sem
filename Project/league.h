#pragma once
#include <iostream>
#include <string>
#include "Division.h"

using namespace std;

class League
{
	private:
		string name;
		Division* first;
		Division* last;
	public:
		League();
		League(string Name);
		~League();
		void add_division(Division &new_division);
		void delete_division(int Id);
		void print();
};

League::League()
{
	first = NULL;
	last = NULL;
	cout<<"League constructed"<<endl<<endl;
}

League::League(string Name)
{
	first = NULL;
	last = NULL;
	name = Name;
	cout<<"League constructed"<<endl<<endl;
}

League::~League()
{
	cout<<"League destructed"<<endl;
}

void League::add_division(Division &new_division)
{
	if(first == NULL)
	{
		first = &new_division;
		last = &new_division;
		cout<<"First division added"<<endl;
		return;
	}

	Division *h = first;
	
	while(h->get_previous() != NULL)
	{
		h = h->get_previous();
		if (new_division.get_id() == h->get_id())
		{
			cout<<"There is already division with this id"<<endl;
			return;
		}
	}
	
	if(last != NULL)
	{
		last->bound(new_division);
		last = &new_division;
		cout<<"New last division added"<<endl;
		return;
	}
}

void League::delete_division(int Id)
{
	if(first->get_id() == Id)
	{
		Division *n;
		n = first;
		first = first->get_previous();
		n->unbound();
		return;
	}
	
	if(last->get_id() == Id)
	{
		Division *n;
		n = last;
		last = last->get_next();
		n->unbound();
		return;
	}
	
	Division *h = first;
	
	while(h != last)
	{
		h = h->get_previous();
		if(h->get_id() == Id)
		{
			h->get_next()->bound(h->get_previous());
			h->unbound();
			return;
		}
	}
	
	cout<<"There is no division with this id"<<endl;
}

void League::print()
{
	cout<<"^^^^^^^^^^^^^^^^^^^^^^"<<endl;
	cout<<"Printing a league:"<<endl;
	cout<<"Name: "<<name<<endl;
	first==NULL ? cout<<"League has no first division"<<endl : cout<<"First division: "<<first->get_name()<<endl;
	last==NULL ? cout<<"League has no last division"<<endl : cout<<"Last division: "<<last->get_name()<<endl;
	cout<<"----------------------"<<endl;
}
