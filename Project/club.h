#pragma once
#include <iostream>
#include <string>
#include "player.h"
#include "staff.h"

using namespace std;

class Club
{
	private:
		string name;
		int id;
		int points;
		int played_matches;
		int goals_for;
		int goals_against;
		int budget;
		struct player_node
		{
			Player* data;
			player_node *next;
		};
		player_node *player_head;
		struct staff_node
		{
			Staff* data;
			staff_node *next;
		};
		staff_node *staff_head;		
	public:
		//constructor destructor
		Club();
		Club(int Id, string Name, int Points, int Played_matches, int Goals_for, int Goals_against, int Budget);
		~Club();
		//rest
		void sell_player(Player &sold_player, Club &purchaser, int fee);
		void add_player(Player &new_player);
		void dismiss_player(int id);
		void add_staff_member(Staff &new_member);
		void dismiss_staff_member(int id);
		void print_player_list();
		void print_staff_list();
		void print();
		
		int get_id();
};

Club::Club()
{
	player_head = NULL;
	staff_head = NULL;
	cout<<"Club constructed"<<endl<<endl;
}

Club::Club(int Id, string Name, int Points, int Played_matches, int Goals_for, int Goals_against, int Budget)
{
	id = Id;
	name = Name;
	points = Points;
	played_matches = Played_matches;
	goals_for = Goals_for;
	goals_against = Goals_against;
	budget = Budget;
	player_head = NULL;
	staff_head = NULL;
	cout<<"Club constructed"<<endl<<endl;
}

Club::~Club()
{
	cout<<"Club destructed"<<endl<<endl;
}


void Club::print_player_list()
{
	if(player_head == NULL)
	{
		cout<<"The list is empty"<<endl<<endl;
		return;
	}
	
	player_node* p = player_head;
	cout<<"^^^^^^^^^^^^^^"<<endl;
	cout<<"Printing list of players of "<<name<<":"<<endl;
	while(p != NULL)
	{
		p->data->print();
		p = p->next;
	}
	cout<<"Printed list of players"<<endl;
	cout<<"--------------"<<endl;
}

void Club::print_staff_list()
{
	if(staff_head == NULL)
	{
		cout<<"The list is empty"<<endl<<endl;
		return;
	}
	
	staff_node* s = staff_head;
	cout<<"^^^^^^^^^^^^^^^"<<endl;
	cout<<"Printnig list of staff members"<<endl;
	while(s != NULL)
	{
		s->data->print();
		s = s->next;
	}
	cout<<"Printed list of staff members"<<endl;
	cout<<"---------------"<<endl;
}

void Club::print()
{
	cout<<"^^^^^^^^^^^^^^^"<<endl;
	cout<<"Club:"<<endl;
	cout<<"Name: "<<name<<endl;
	cout<<"Id: "<<id<<endl;
	cout<<"Amount of points: "<<points<<endl;
	cout<<"Matches played: "<<played_matches<<endl;
	cout<<"Goals for: "<<goals_for<<endl;
	cout<<"Goals against: "<<goals_against<<endl;
	cout<<"Budget: "<<budget<<" euro"<<endl<<endl;
	cout<<"---------------"<<endl;
}

void Club::add_player(Player &new_player)
{
	if(new_player.get_is_employed())
	{
		cout<<"This player is already employed"<<endl<<endl;
		return;
	}
	
	player_node* p_h = player_head;
	while(p_h != NULL)
	{
		if(new_player.get_id() == p_h->data->get_id())
			cout<<"Can't add this player, somebody already has his id"<<endl<<endl;
		p_h = p_h->next;
	}
	
	player_node* p = new player_node;
	p->data = &new_player;
	p->next = player_head;
	p->data->set_is_employed(true);
	player_head = p;
}

void Club::add_staff_member(Staff &new_staff)
{
	staff_node* s_h = staff_head;
	while(s_h != NULL)
	{
		if(new_staff.get_id() == s_h->data->get_id())
			cout<<"Can't add this staff member, somebody already has his id"<<endl<<endl;
		s_h = s_h->next;
	}
	staff_node* s = new staff_node;
	s->data = &new_staff;
	s->next = staff_head;
	staff_head = s;
}

void Club::dismiss_player(int Id)
{
	if(player_head == NULL)
	{
		cout<<"The list is empty"<<endl<<endl;
		return;
	}
	
	player_node* p_h = player_head;

	if(p_h->data->get_id() == Id)
	{
		if(p_h->next == NULL)
		{
			p_h->data->set_is_employed(false);
			delete p_h;
			player_head = NULL;
			cout<<"Player dismissed"<<endl<<endl;
		}
		else
		{
			p_h->data->set_is_employed(false);
			player_node* np_h = player_head->next;
			delete p_h;
			player_head = np_h;
		}
		cout<<"Player dismissed"<<endl<<endl;
		return;
	}
	
	while(p_h->next != NULL)
	{
		player_node *pp_h = p_h;
		p_h = p_h->next;
		if(p_h->data->get_id() == Id)
		{
			p_h->data->set_is_employed(false);
			pp_h->next = p_h->next;
			delete p_h;
			cout<<"Player dismissed"<<endl<<endl;
			return;
		}
	}
	
	cout<<"There is no player with this id"<<endl<<endl;
}

void Club::dismiss_staff_member(int Id)
{
	if(staff_head == NULL)
	{
		cout<<"The list is empty"<<endl<<endl;
		return;
	}
	
	staff_node* s_h = staff_head;

	if(s_h->data->get_id() == Id)
	{
		if(s_h->next == NULL)
		{
			delete s_h;
			staff_head = NULL;
			cout<<"Staff member dismissed"<<endl<<endl;
		}
		else
		{
			staff_node* ns_h = staff_head->next;
			delete s_h;
			staff_head = ns_h;
		}
		cout<<"Staff member dismissed"<<endl<<endl;
		return;
	}
	
	while(s_h->next != NULL)
	{
		staff_node *ps_h = s_h;
		s_h = s_h->next;
		if(s_h->data->get_id() == Id)
		{
			ps_h->next = s_h->next;
			delete s_h;
			cout<<"Staff member dismissed"<<endl<<endl;
			return;
		}
	}
	
	cout<<"There is no staff member with this id"<<endl<<endl;
	
}

void Club::sell_player(Player &Sold_player, Club &Purchaser, int fee)
{
	this->dismiss_player(Sold_player.get_id());
	Purchaser.add_player(Sold_player);
	this->budget += fee;
	Purchaser.budget -= fee;
}

int Club::get_id()
{
	return id;
}
