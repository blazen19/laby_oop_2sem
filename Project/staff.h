#pragma once
#include <iostream>
#include <string>
#include <ctime>
using namespace std;

class Staff
{
	private:
		int id;
		string name;
		string surname;
		string occupation;
		int year_of_birth;
	public:
		Staff();
		Staff(int Id, string Name, string Surname, string Occupation, int Year_of_birth);
		~Staff();
		void print();
		int calculate_age();
		int get_id();
};

Staff::Staff(int Id, string Name, string Surname, string Occupation, int Year_of_birth)
{
	id = Id;
	name = Name;
	surname = Surname;
	occupation = Occupation;
	year_of_birth = Year_of_birth;
	cout<<"Staff member constrcted"<<endl<<endl;
}

Staff::Staff()
{
	cout<<"Staff member constrcted"<<endl<<endl;
}

Staff::~Staff()
{
	cout<<"Staff member destructed"<<endl<<endl;
}

void Staff::print()
{
	cout<<"^^^^^^^^^^^^"<<endl;
	cout<<"Staff member:"<<endl;
	cout<<"Id: "<<id<<endl;
	cout<<"Name: "<<name<<endl;
	cout<<"Surname: "<<surname<<endl;
	cout<<"Year of birth: "<<year_of_birth<<endl;
	cout<<"Age: "<<calculate_age()<<endl<<endl;
	cout<<"------------"<<endl;
}

int Staff::calculate_age()
{
	return 2000 + (time(NULL)/60/60/24/365-30) - year_of_birth;
}

int Staff::get_id()
{
	return id;
}
