#include <iostream>
#include "league.h"

using namespace std;

int main()
{
	cout<<"To demonstrate the basic functionality of this programme, several functions will be invoked in proper way (I will not for example add two players with the same id, which would not be allowed by programme)"<<endl;
	//players
	Player a(1, "A", "A", "ST", 181, 100000, 1, "Aa", 1990, 0);
	Player b(2, "B", "B", "CM", 178, 250000, 1, "Ba", 1996, 0);
	Player c(3, "C", "C", "LM", 171, 20000, 1, "Ba", 1993, 0);
	Player d(4, "D", "D", "CB", 189, 70000, 1, "Ba", 1994, 0);
	cout<<"Four players were constructed, printing one of them"<<endl;
	a.print();
	//staff
	cout<<"clubs consists not only from players but also from staff members"<<endl;
	Staff sa(1, "sa", "sa", "Manager", 1965);
	sa.print();
	//clubs
	cout<<"More operations are possible with clubs"<<endl;
	Club fa(1, "FCA", 0, 0, 0, 0, 200000);
	Club fb(2, "FCB", 0, 0, 0, 0, 200000);
	fa.add_player(a);
	fa.add_player(b);
	fa.add_player(c);
	fa.add_staff_member(sa);
	cout<<"Printing FCA club"<<endl;
	fa.print();
	cout<<"FCA got three players, please notice that now they are considered employed now"<<endl;
	fa.print_player_list();
	cout<<"FCA decided to dismiss B player"<<endl;
	fa.dismiss_player(2);
	fa.print_player_list();
	cout<<"And sell C player to FCB"<<endl;
	fa.sell_player(c, fb, 25000);
	fa.print_player_list();
	fb.print_player_list();
	cout<<"FCB decided to hire sa as their new manager"<<endl;
	fb.print_staff_list();
	//divisions
	cout<<"Clubs are gathered by divisions"<<endl;
	Division da("da", 1);
	Division db("db", 2);
	da.add_club(fa);
	db.add_club(fb);
	cout<<"FCA was signed to da division, FCB to db division"<<endl;
	da.print();
	db.print();
	cout<<"Divisions may be linked, so when a club wins lower division it is promoted to higher one. Clubs can be linked without league, but a league will be constructed now for convinience"<<endl;
	//league
	League la("La");
	la.add_division(da);
	la.add_division(db);
	cout<<"Now da is the highest division in the league"<<endl;
	la.print();
	da.print();
	cout<<"As can be seen above, da is now higher league than db"<<endl;
	cout<<"If the club in db is promoted, it is signed to da"<<endl;
	db.promote(2);
	da.print();
	db.print();
	cout<<"As can be seen, now all clubs are in da, while no clubs are in db"<<endl<<endl;
	
	return 0;
}
