#pragma once
#include <iostream>
#include <string>
#include <ctime>

using namespace std;

class Player
{
	private:
		int id;
		string name;
		string surname;
		string position; //GK - goalkeeper, RB - right back, CB - central back, LB - left back, CM - central midfielder, LW - left wing, RW - right wing, ST - striker are the most common, but there is more
		int height; //in cm
		int value; //in Euro
		bool right_footed; //true if right-footed, false if left-footed
		string nationality;
		int year_of_birth;
		int goals_in_season;
		bool is_employed; //so he cannot be in two different teams
	public:
		//constructors, destructors, operators
		Player();
		Player(int Id, string Name, string Surname, string Position, int Height, int Value, bool Right_footed, string Nationality, int Year_of_birth, int Goals_in_season);
		~Player();
		//Player& operator=(const Player& Player);
		//handy functions
		void print();
		void add_goals(int goal_num);
		int calculate_age();
		//getters
		int get_id() const;
		string get_name() const;
		string get_surname() const;
		string get_position() const;
		int get_height() const;
		int get_value() const;
		bool get_right_footed() const;
		string get_nationality() const;
		int get_year_of_birth() const;
		int get_goals_in_season() const;
		bool get_is_employed() const;
		//setters
		void set_id(int Id);
		void set_name (string Name); // rather unhelpful, but essential when a player was constructed but didn't get a name, only few setters will be called more than once
		void set_surname (string Surname);
		void set_position (string Position); //can happen, Piszczek went from striker to right back
		void set_height (int Height);
		void set_value (int Value);
		void set_right_footed (bool Right_footed);
		void set_nationality (string Nationality); //Diego Costa was a Brazilian in 2013, from 2014 he's a Spaniard
		void set_year_of_birth (int Year_of_birth);
		void set_goals_in_season (int Goals_in_season);
		void set_is_employed(bool Is_he);
};

Player::Player()
{
	cout<<"player constructed"<<endl<<endl;
}

Player::Player(int Id, string Name, string Surname, string Position, int Height, int Value, bool Right_footed, string Nationality, int Year_of_birth, int Goals_in_season)
{
	id = Id;
	name = Name;
	surname = Surname;
	position = Position;
	height = Height;
	value = Value;
	right_footed = Right_footed;
	nationality = Nationality;
	year_of_birth = Year_of_birth;
	goals_in_season = Goals_in_season;
	is_employed = 0;
	cout<<"player constructed"<<endl<<endl;
}

Player::~Player()
{
	cout<<"player destructed"<<endl<<endl;
}


int Player::get_id() const
{
	return id;
}
string Player::get_name() const
{
	return name;
}
string Player::get_surname() const
{
	return surname;
}
string Player::get_position() const
{
	return position;
}
int Player::get_height() const
{
	return height;
}
int Player::get_value() const
{
	return value;
}
bool Player::get_right_footed() const
{
	return right_footed;
}
string Player::get_nationality() const
{
	return nationality;
}
int Player::get_year_of_birth() const
{
	return year_of_birth;
}
int Player::get_goals_in_season() const
{
	return goals_in_season;
}
bool Player::get_is_employed() const
{
	return is_employed;
}
void Player::set_id(int Id)
{
	id = Id;
}
void Player::set_name (string Name)
{
	name = Name;
}
void Player::set_surname (string Surname)
{
	surname = Surname;
}
void Player::set_position (string Position)
{
	position = Position;
}
void Player::set_height (int Height)
{
	height = Height;
}
void Player::set_value (int Value)
{
	value = Value;
}
void Player::set_right_footed (bool Right_footed)
{
	right_footed = Right_footed;
}
void Player::set_nationality (string Nationality)
{
	nationality = Nationality;
}
void Player::set_year_of_birth (int Year_of_birth)
{
	year_of_birth = Year_of_birth;
}
void Player::set_goals_in_season (int Goals_in_season)
{
	goals_in_season = Goals_in_season;
}
void Player::set_is_employed(bool Is_he)
{
	is_employed = Is_he;
}
void Player::print()
{
	cout<<"^^^^^^^^^^^^"<<endl;
	cout<<"Player:"<<endl;
	cout<<"Id: "<<id<<endl;
	cout<<"Name: "<<name<<endl;
	cout<<"Surname: "<<surname<<endl;
	cout<<"Position: "<<position<<endl;
	cout<<"Height: "<<height<<" cm"<<endl;
	cout<<"Value: "<<value<<" euro"<<endl; 
	cout<<"Better foot: ";
	right_footed ? cout<<"Right"<<endl:cout<<"Left"<<endl;
	cout<<"Nationality: "<<nationality<<endl;
	cout<<"Year of birth: "<<year_of_birth<<endl;
	cout<<"Age: "<<calculate_age()<<endl;
	cout<<"Goals in season: "<<goals_in_season<<endl;
	is_employed? cout<<"Employed"<<endl<<endl : cout<<"Unemployed"<<endl<<endl;
	cout<<"------------"<<endl;
}

void Player::add_goals(int goal_num)
{
	goals_in_season += goal_num;
}

int Player::calculate_age()
{
	return 2000 + (time(NULL)/60/60/24/365-30) - year_of_birth;
}

