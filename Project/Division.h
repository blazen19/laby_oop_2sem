#pragma once
#include <iostream>
#include <string>
#include "club.h"

using namespace std;

class Division
{
	private:
		int id;
		string name;
		Division* next;
		Division* previous;
		struct club_node
		{
			Club *data;
			club_node *next;
		};
		club_node *club_head;
	public:
		Division();
		Division(string Name, int Id);
		~Division();
		
		void add_club(Club &new_club);
		void delete_club(int Id);
		void promote(int Id);
		void relegate(int Id);
		void print();
		void bound(Division &Lower_div);
		void bound(Division *Lower_div);
		void bound_up(Division &Lower_div);
		void bound_down(Division &Upper_div);
		void unbound();
		Division* get_next();
		Division* get_previous();
		int get_id();
		string get_name();
};

Division::Division()
{
	Division* next = NULL;
	Division* previous = NULL;
	club_head = NULL;
	cout<<"Division constructed"<<endl<<endl;
}

Division::Division(string Name, int Id)
{
	name = Name;
	id = Id;
	next = NULL;
	previous = NULL;
	club_head = NULL;
	cout<<"Division constructed"<<endl<<endl;
}

Division::~Division()
{
	cout<<"Division destructed"<<endl<<endl;
}

void Division::add_club(Club &new_club)
{
	club_node* c_h = club_head;
	while(c_h != NULL)
	{
		if(new_club.get_id() == c_h->data->get_id())
			cout<<"Can't add this club, some already has this id"<<endl<<endl;
		c_h = c_h->next;
	}
	
	club_node* c = new club_node;
	c->data = &new_club;
	c->next = club_head;
	club_head = c;
}

void Division::promote(int Id)
{
	if(next == NULL)
	{
		cout<<"Club cannot be promoted from the first league, it becomes the champion"<<endl<<endl;
		return;
	}
	
	if(club_head == NULL)
	{
		cout<<"There is no club to promote"<<endl<<endl;
		return;
	}
	
	club_node* c_h = club_head;
	
	if(c_h->data->get_id() == Id)
	{
		if(c_h->next == NULL)
		{
			this->next->add_club(*(c_h->data));
			delete_club(Id);
			return;
		}
		else
		{
			club_node* nc_h = club_head->next;
			this->next->add_club(*(c_h->data));
			delete_club(Id);
			return;
		}
		cout<<"Club promoted"<<endl<<endl;
		return;
	}
	
	while(c_h->next != NULL)
	{
		c_h = c_h->next;
		if(c_h->data->get_id() == Id)
		{
			this->next->add_club(*(c_h->data));
			delete_club(Id);
			return;
		}
	}
	cout<<"There is no club with this id"<<endl<<endl;	
}

void Division::relegate(int Id)
{
	if(previous == NULL)
	{
		cout<<"Club cannot be relected from the lowest league"<<endl<<endl;
		return;
	}
	
	if(club_head == NULL)
	{
		cout<<"There is no club to relegate"<<endl<<endl;
		return;
	}
	
	club_node* c_h = club_head;
	
	if(c_h->data->get_id() == Id)
	{
		if(c_h->next == NULL)
		{
			this->previous->add_club(*(c_h->data));
			delete_club(Id);
			return;
		}
		else
		{
			club_node* nc_h = club_head->next;
			this->previous->add_club(*(c_h->data));
			delete_club(Id);
			return;
		}
		cout<<"Club relegated"<<endl<<endl;
		return;
	}
	
	while(c_h->next != NULL)
	{
		c_h = c_h->next;
		if(c_h->data->get_id() == Id)
		{
			this->previous->add_club(*(c_h->data));
			delete_club(Id);
			return;
		}
	}
	cout<<"There is no club with this id"<<endl<<endl;	
}

void Division::delete_club(int Id)
{
	if(club_head == NULL)
	{
		cout<<"The list is empty"<<endl<<endl;
		return;
	}
	
	club_node* c_h = club_head;

	if(c_h->data->get_id() == Id)
	{
		if(c_h->next == NULL)
		{
			delete c_h;
			club_head = NULL;
		}
		else
		{
			club_node* nc_h = club_head->next;
			delete c_h;
			club_head = nc_h;
		}
		cout<<"Club deleted"<<endl<<endl;
		return;
	}
	
	while(c_h->next != NULL)
	{
		club_node *pc_h = c_h;
		c_h = c_h->next;
		if(c_h->data->get_id() == Id)
		{
			pc_h->next = c_h->next;
			delete c_h;
			cout<<"Club deleted"<<endl<<endl;
			return;
		}
	}
	
	cout<<"There is no club with this id"<<endl<<endl;	
}

void Division::print()
{
	cout<<"^^^^^^^^^^^^^^^^^^"<<endl;
	cout<<"Printing division"<<endl;
	cout<<"Id: "<<id<<endl;
	cout<<"Name: "<<name<<endl;
	cout<<"Promotion to: ";
	next == NULL ? cout<<"N/a"<<endl : cout<<next->name<<endl;
	cout<<"Relegtion to: ";
	previous == NULL ? cout<<"N/a"<<endl : cout<<previous->name<<endl;
	if(club_head == NULL)
	{
		cout<<"There are no clubs"<<endl;
	}
	else
	{
		club_node* c = club_head;
		cout<<"Printing list of clubs:"<<endl;
		while(c != NULL)
		{
			c->data->print();
			c = c->next;
		}
		cout<<"Printed list of clubs"<<endl;
	}
	cout<<"------------------"<<endl;
	cout<<endl;
}

void Division::bound(Division &Lower_div)
{
	this->bound_down(Lower_div);
	Lower_div.bound_up(*this);
}

void Division::bound(Division *Lower_div)
{
	this->bound_down(*Lower_div);
	Lower_div->bound_up(*this);
}

void Division::bound_down(Division &Lower_div)
{
	Lower_div.next = this;
}
void Division::bound_up(Division &Upper_div)
{
	Upper_div.previous = this;
}

void Division::unbound()
{
	if(previous != NULL)
		previous->next = NULL;
	if(next != NULL)
		next->previous = NULL;
	next = NULL;
	previous = NULL;
}

Division* Division::get_next()
{
	return next;
}

Division* Division::get_previous()
{
	return previous;
}

int Division::get_id()
{
	return id;
}

string Division::get_name()
{
	return name;
}
