
#include <iostream>
#include <cstring>

using namespace std;

class Movie{
private:
	int id;
	char* title;
	int year; 
	Movie *next; 
public:
	Movie(int id, char* title, int year);
	~Movie();
	void print();
	Movie* getnext() const;
	char* gettitle() const;
	int getyear() const;
	int getid() const;
	void setnext(Movie *Nnext);
	void settitle(char* Ntitle);
	void setyear(int Nyear);
	void setid(int Nid);
};

class MovieList
{
private:
	Movie* head;
public:
	MovieList();
	~MovieList();
	Movie* gethead() const;
	MovieList(const MovieList& SMovielist);
	MovieList(const MovieList* SMovielist);
	MovieList& operator=(const MovieList& SMovielist);
	MovieList& operator=(const MovieList* SMovielist);
	friend MovieList& operator+(const MovieList& L, const MovieList& R);
	void add_Movie(int id, char* title, int year);
	void remove_Movie(int id); 
	void print();
};

Movie* MovieList::gethead() const
{
    return head;
}
Movie* Movie::getnext() const
{
    return next;
}
char* Movie::gettitle() const
{
   return title;
}
int Movie::getyear() const
{
    return year;
}
int Movie::getid() const
{
    return id;
}
void Movie::setnext(Movie *Nnext)
{
	next = Nnext;
}
void Movie::settitle(char* Ntitle)
{
	title = Ntitle;
}
void Movie::setyear(int Nyear)
{
	year = Nyear;
}
void Movie::setid(int Nid)
{
	id = Nid;
}

Movie::Movie(int Nid, char* Ntitle, int Nyear)
{
	id = Nid;
	title = Ntitle;
	year = Nyear;
	next = NULL;
	cout<<"Movie constructed"<<endl;
	print();
}

Movie::~Movie()
{
	cout<<"Movie destructed"<<endl;
	print();
}

MovieList::MovieList()
{
	head = NULL;
	cout<<"New list of movies"<<endl;
}

MovieList::~MovieList()
{
	cout<<"##################"<<endl;
    cout<<"Deleting a list"<<endl;
    Movie *ptr = head;
    if(head != NULL)
    {
        while(true)
        {
            ptr = ptr->getnext();
            delete head;
            if(ptr == NULL)
            {
                break;
            }
            head = ptr;
        }
    }
    cout<<"List deleted"<<endl;
	cout<<"##################"<<endl<<endl;
}

void Movie::print()
{
	cout<<"Movie's data:"<<endl;
	cout<<"Title: "<<title<<endl;
	cout<<"Id: "<<id<<endl;
	cout<<"Year: "<<year<<endl<<endl;
}

void MovieList::print()
{
	cout<<"^^^^^^^^^^^^^^^^^"<<endl;
	cout<<"Printing List:"<<endl;
	
	Movie* ptr = head;
	
	while(ptr!=NULL)
	{
		ptr->print();
		ptr = ptr->getnext();
	}
	cout<<"List printed"<<endl;
	cout<<"-----------------"<<endl<<endl;
}

void MovieList::add_Movie(int id, char* title, int year)
{
	if(head==NULL)
	{
	    Movie* NMovie = new Movie(id, title, year);
		head = NMovie;
		NMovie->setnext(NULL);
		return;
	}

	Movie* ptr = head;
	while(true)
    {
        if(ptr->getid()==id)
        {
            cout<<"There is a movie with this id"<<endl<<endl;
            return;
        }
        else if(ptr->getnext()== NULL)
        {
            break;
        }
        ptr = ptr->getnext();
    }

    Movie* NMovie = new Movie(id, title, year);
    ptr = head;
    Movie* ptrp = NULL;

    while(true)
    {
        if(strcmp(title, ptr->gettitle())<0 && ptr == head)
        {
            head = NMovie;
            NMovie->setnext(ptr);
            return;
        }
        else if(strcmp(title, ptr->gettitle())<0)
        {
            ptrp->setnext(NMovie);
            NMovie->setnext(ptr);
            return;
        }
        else if(ptr->getnext() == NULL)
        {
            ptr->setnext(NMovie);
            NMovie->setnext(NULL);
            return;
        }
        ptrp = ptr;
        ptr = ptrp->getnext();
    }
}

MovieList::MovieList(const MovieList& SMovielist)
{
    head = NULL;
    Movie *ptr = SMovielist.gethead();
    Movie* ptrp = NULL;

    while(ptr!=NULL)
    {
        int id = ptr->getid();
        char* title = ptr->gettitle();
        int year = ptr->getyear();
        Movie* NMovie = new Movie(id, title, year);
        if(head==NULL)
        {
            head = NMovie;
        }

        NMovie->setnext(NULL);

        if(ptrp!=NULL)
        {
            ptrp->setnext(NMovie);
        }

        ptrp = NMovie;
        ptr = ptr->getnext();
    }
}

MovieList::MovieList(const MovieList* SMovielist)
{
    head = NULL;
    Movie *ptr = SMovielist->gethead();
    Movie* ptrp = NULL;

    while(ptr!=NULL)
    {
        int id = ptr->getid();
        char* title = ptr->gettitle();
        int year = ptr->getyear();
        Movie* NMovie = new Movie(id, title, year);
        if(head==NULL)
        {
            head = NMovie;
        }

        NMovie->setnext(NULL);

        if(ptrp!=NULL)
        {
            ptrp->setnext(NMovie);
        }

        ptrp = NMovie;
        ptr = ptr->getnext();
    }
}

MovieList& MovieList::operator=(const MovieList& SMovielist)
{
    head = NULL;
    Movie *ptr = SMovielist.gethead();
    Movie* ptrp = NULL;

    while(ptr!=NULL)
    {
        int id = ptr->getid();
        char* title = ptr->gettitle();
        int year = ptr->getyear();
        Movie* NMovie = new Movie(id, title, year);
        if(head==NULL)
        {
            head = NMovie;
        }

        NMovie->setnext(NULL);

        if(ptrp!=NULL)
        {
            ptrp->setnext(NMovie);
        }

        ptrp = NMovie;
        ptr = ptr->getnext();
    }
}

MovieList& MovieList::operator=(const MovieList* SMovielist)
{
    head = NULL;
    Movie *ptr = SMovielist->gethead();
    Movie* ptrp = NULL;

    while(ptr!=NULL)
    {
        int id = ptr->getid();
        char* title = ptr->gettitle();
        int year = ptr->getyear();
        Movie* NMovie = new Movie(id, title, year);
        if(head==NULL)
        {
            head = NMovie;
        }

        NMovie->setnext(NULL);

        if(ptrp!=NULL)
        {
            ptrp->setnext(NMovie);
        }

        ptrp = NMovie;
        ptr = ptr->getnext();
    }
}

MovieList& operator+(const MovieList& L, const MovieList& R)
{
	Movie *ptr = R.head;
    
    MovieList N(L);
    
    while(ptr!=NULL)
    {
        int id = ptr->getid();
        char* title = ptr->gettitle();
        int year = ptr->getyear();
    	N.add_Movie(id, title, year);
       	ptr = ptr->getnext();
	}
	return N;
}

int main()
{
	MovieList a;
	a.add_Movie(2, "b", 2000);
	a.add_Movie(1, "a", 2000);
	
	MovieList c;
	c.add_Movie(2, "b", 2000);
	c.add_Movie(3, "c", 2000);
	
	MovieList b;
	
	b = a + c;
	
	b.print();
	
	return 0;
}
