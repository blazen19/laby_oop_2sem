//Copy constructor and operator=
Movie* MovieList::getHead() const
{
    return head;
}

MovieList::MovieList(const MovieList& someMovielist)
{
    head = NULL;
    Movie *ptr = someMovielist.getHead();
    Movie* pptr = NULL;

    while(pointer!=nullptr)
    {
        int id = pointer->getid();
        char* title = pointer->gettitle();
        long year = pointer->getyear();
        Movie* myMovie = new Movie(id, title, year);
        if(head==nullptr)
        {
            head = myMovie;
        }

        myMovie->setnext(nullptr);

        if(pptr!=nullptr)
        {
            pptr->setnext(myMovie);
        }

        pptr = myMovie;
        //No need to sort, already sorted
        pointer = pointer->getnext();
    }
}

MovieList::MovieList(const MovieList* someMovielist)
{
    head = nullptr;
    Movie *pointer = someMovielist->getHead();
    Movie* pptr = nullptr;

    while(pointer!=nullptr)
    {
        int id = pointer->getid();
        char* title = pointer->gettitle();
        long year = pointer->getyear();
        Movie* myMovie = new Movie(id, title, year);
        if(head==nullptr)
        {
            head = myMovie;
        }

        myMovie->setnext(nullptr);

        if(pptr!=nullptr)
        {
            pptr->setnext(myMovie);
        }

        pptr = myMovie;
        //No need to sort, already sorted
        pointer = pointer->getnext();
    }
}

MovieList& MovieList::operator=(const MovieList& someMovielist)
{
    head = nullptr;
    Movie *pointer = someMovielist.getHead();
    Movie* pptr = nullptr;

    while(pointer!=nullptr)
    {
        int id = pointer->getid();
        char* title = pointer->gettitle();
        long year = pointer->getyear();
        Movie* myMovie = new Movie(id, title, year);
        if(head==nullptr)
        {
            head = myMovie;
        }

        myMovie->setnext(nullptr);

        if(pptr!=nullptr)
        {
            pptr->setnext(myMovie);
        }

        pptr = myMovie;
        pointer = pointer->getnext();
    }
}

MovieList& MovieList::operator=(const MovieList* someMovielist)
{
    head = nullptr;
    Movie *pointer = someMovielist->getHead();
    Movie* pptr = nullptr;

    while(pointer!=nullptr)
    {
        int id = pointer->getid();
         char* title = pointer->gettitle();
        long year = pointer->getyear();
        Movie* myMovie = new Movie(id, title, year);
        if(head==nullptr)
        {
            head = myMovie;
        }

        myMovie->setnext(nullptr);

        if(pptr!=nullptr)
        {
            pptr->setnext(myMovie);
        }

        pptr = myMovie;
        //No need to sort, already sorted
        pointer = pointer->getnext();
    }
}

