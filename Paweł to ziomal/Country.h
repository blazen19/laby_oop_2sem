#pragma once
#include <iostream>
#include <cstring>

//301106 Pawel Borsukiewicz

using namespace std;

class Country{
private:
	int key;
	const char* name;
	long population;
	Country* next;
public:
	Country(int key, const char* name, long population); //prints data of the country
	Country(const Country& somecountry); //Copy constructor
	Country(const Country* somecountry); //Copy constructor with pointer
	~Country(); //prints data of the country
	Country& operator=(const Country& somecountry); //Operator=
	Country& operator=(const Country* somecountry); //Operator=
	void print(); //prints data of the country
	void setNext(Country *Cnext);
	void setName(const char* Cname);
	void setPopulation(long Cpopulation);
	void setKey(int Ckey);
	Country* getNext() const;
	const char* getName() const;
	long getPopulation() const;
	int getKey() const;
};

