#include <iostream>
#include <cstring>
#include "Country.h"

//301106 Pawel Borsukiewicz

using namespace std;

Country::Country(int Ckey, const char* Cname, long Cpopulation)//prints data of the country
{
	key = Ckey;
	name = Cname;
	population = Cpopulation;
	next = nullptr;
	cout<<"----------------------------"<<endl;
	cout<<"Constructor of COUNTRY activated"<<endl;
	print();
    cout<<"----------------------------"<<endl<<endl;
}

Country::~Country() //prints data of the country
{
    cout<<"----------------------------"<<endl;
	cout<<"Destructor of COUNTRY activated"<<endl;
	print();
	cout<<"----------------------------"<<endl<<endl;
}

void Country::print() //prints data of the country
{
	cout<<"Print function"<<endl;
	cout<<"My country's data:"<<endl;
	cout<<"Name: "<<name<<endl;
	cout<<"Key: "<<key<<endl;
	cout<<"Population: "<<population<<endl<<endl;
}
//Setters
void Country::setNext(Country *Cnext)
{
	next = Cnext;
}
void Country::setName(const char* Cname)
{
	name = Cname;
}
void Country::setPopulation(long Cpopulation)
{
	population = Cpopulation;
}
void Country::setKey(int Ckey)
{
	key = Ckey;
}
//Getters
Country* Country::getNext() const
{
    return next;
}
const char* Country::getName() const
{
   return name;
}
long Country::getPopulation() const
{
    return population;
}
int Country::getKey() const
{
    return key;
}

//Copy constructors and operator=

Country::Country(const Country& somecountry)
{
    key = somecountry.getKey();
	name = somecountry.getName();
	population = somecountry.getPopulation();
	next = somecountry.getNext();
}

Country::Country(const Country* somecountry)
{
    key = somecountry->getKey();
	name = somecountry->getName();
	population = somecountry->getPopulation();
	next = somecountry->getNext();
}

Country& Country::operator=(const Country& somecountry)
{
    key = somecountry.getKey();
	name = somecountry.getName();
	population = somecountry.getPopulation();
	next = somecountry.getNext();
}

Country& Country::operator=(const Country* somecountry)
{
    key = somecountry->getKey();
	name = somecountry->getName();
	population = somecountry->getPopulation();
	next = somecountry->getNext();
}
