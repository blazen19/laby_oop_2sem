#pragma once
#include <iostream>
#include <cstring>

//301106 Pawel Borsukiewicz

using namespace std;

//Forward declaration
class Country;

class CountryList
{
private:
	Country* head;
public:
	CountryList();//Initialize empty list null to head
	CountryList(const CountryList& somecountrylist); //Copy constructor
	CountryList(const CountryList* somecountrylist); //Copy constructor pointer
	~CountryList();//Delete all elements
	CountryList& operator=(const CountryList& somecountrylist); //Operator=
	CountryList& operator=(const CountryList* somecountrylist); //Operator= pointer
	void add_Country(int key, const char* name, long population); //if country does not exist, create it and add to the list, sort by names
	void removeCountry(int key); //removes fro the list and deletes
	void print(); //prints whole list
	Country* getHead() const;
};


