#include <iostream>
#include <cstring>

using namespace std;
struct Movie 
{ 
	int id;
	char* title;
	int year; 
	Movie *next; 
}; 

struct Movie* head = NULL; 

void add_Movie(int id, char *title, int year) 
{
	Movie* tmp = new Movie;
	int i=0;
	tmp->id = id;
	tmp->title = new char [strlen(title) + 1];
	
	strcpy(tmp->title, title); 
	tmp->year = year;
	Movie* ptr;
	
	ptr = head;
	while (ptr != NULL) 
	{ 
		if(ptr->id==id)
		{
			cerr<<"Two movies can't have the same id\n";
			return;
		}
		ptr = ptr->next; 
	}
	
	ptr = head;
	while (ptr != NULL && strcmp(title, ptr->title)>0) 
	{
		i++;
		ptr = ptr->next;
	} 
	
	ptr = head;
	for(int j=0; j<i-1; j++)
		ptr = ptr->next;
	
	if(i==0)
	{
		tmp->next = head; 
   		head = tmp;
		return;
	}
	if(ptr != NULL)
	{ 
		tmp->next = ptr->next; 
		ptr->next = tmp;
   		return;
	}
}

void remove_Movie(int id)
{
	Movie *ptr = head;
	if(head->id == id)
	{
		head = head->next;
		delete [] (ptr->title);
		delete(ptr);
		return;
	}
	
	int i=0;
	while(ptr->id != id)
	{
		ptr = ptr->next;
		i++;
		
		if(ptr == NULL)
		{
			cerr<<"There is no movie with such id"<<endl;	
			return;
		}
	}
	
	ptr = head;
	for(i; i>1; i--)
		ptr = ptr->next;
	Movie *ptr2;
	ptr2 = ptr->next;
	ptr->next = ptr2->next;
	delete [] (ptr2->title);
	delete (ptr2);
}

int & access(int id, char * title)
{
	Movie* tmp; // = new Movie;
	tmp = head;
	while(tmp != NULL)
	{
		if(tmp -> id == id)
		{
			if(strcmp(tmp -> title, title) != 0)
			{
				cout<<"The title is being updated"<<endl;
				int year = tmp -> year;
				remove_Movie(id);
				Movie* tmp = new Movie;
				add_Movie(id, title, year);
				tmp = head;
				while(tmp != NULL)
				{
					if(tmp -> id == id)
						return tmp -> year;
					tmp = tmp -> next;
				}
			}
		}
		tmp = tmp -> next;
	}
	
	cout<<"There is no film with such id"<<endl;
	
	add_Movie(id, title, 1900);
	
	tmp = head;
	while(tmp != NULL)
	{
		if(tmp -> id == id)
		{
			return tmp->year;
		}
		tmp = tmp -> next;
	}
}  

void print() 
{ 
	Movie* ptr;
	ptr = head;
	cout<<endl;	
	cout<<"The list:"<<endl;
	cout<<endl;
	while(ptr != NULL) 
	{ 
		cout<<"Id: "<< ptr->id <<endl<<"Title: "<< ptr->title<<endl<<"Year: "<<ptr->year<<endl;
		cout<<endl; 
		ptr = ptr->next; 
	}
} 

int main() 
{
	int c=1;
	
	int id, year;
	char *name;
	
	while(c)
	{
		cout<<"What would you like to do?"<<endl<<"1 - Add a movie"<<endl<<"2 - Remove a movie"<<endl<<"3 - Print the list"<<endl<<"4 - demonstration"<<endl<<"0 - Exit"<<endl<<"Choise: ";
		cin>>c;
		
		if(c == 1)
		{
			cout<<"Id: ";
			cin>>id;
			cout<<"Title: ";
			//name = new char[50];
			cin>>name;
			cout<<"Year: ";
			cin>>year;
			add_Movie(id, "fdsfsafdsa", year);
		}
		
		if(c == 2)
		{
			int id;
			cout<<"Id: ";
			cin>>id;
			remove_Movie(id);
		}
		if(c == 3)
			print();
		if(c == 4)
		{
			add_Movie(1, "aa", 1990);
			add_Movie(2, "da", 1996);
			add_Movie(3, "fa", 1992);
			add_Movie(6, "ba", 1994);
			print();
			access(3, "fa")++;
			access(8, "ab");
			print();
		}
		cout<<endl;
	}
		
	return 0; 
} 
